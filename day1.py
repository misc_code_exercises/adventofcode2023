# Advent of code 2023 day 1
# https://adventofcode.com/2023/day/1
import math


def get_digits(string):
    res = ""
    for c in convert_digits(string):
        if c.isdigit():
            res += c
    if len(res) == 1:
        res += res
    elif len(res) > 2:
        res = res[0] + res[-1]
    return res


def convert_digits(string):
    digit_dict = {
        "one": '1e',
        "two": '2o',
        "three": '3e',
        "four": '4',
        "five": '5e',
        "six": '6',
        "seven": '7n',
        "eight": '8t',
        "nine": '9e',
    }
    res = math.inf
    first_digit = ""
    for digit in digit_dict:
        pos = string.find(digit)
        if pos != -1 and pos < res:
            res = pos
            first_digit = digit
    if res == math.inf:
        return string
    else:
        string = string.replace(first_digit, digit_dict[first_digit])
        return convert_digits(string)


def get_sum_from_file(input):
    num_list = []
    file = open(input, 'r')
    for line in file:
        num_list.append(int(get_digits(line)))
    file.close()
    return sum(num_list)
