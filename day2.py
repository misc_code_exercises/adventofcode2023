# Advent of code 2023 day 2
# https://adventofcode.com/2023/day/2
MAX_RED = 12
MAX_GREEN = 13
MAX_BLUE = 14


def parse_game_line(line):
    game_id = line.split(":")[0].split("Game ")[-1]
    draws = line.split("\n")[0].split(": ")[-1].split("; ")
    return game_id, draws


def check_draws(draws):
    for d in draws:
        subdraw = d.split(", ")
        for sd in subdraw:
            count, color = sd.split()
            if color == "blue" and int(count) > MAX_BLUE:
                return False
            elif color == "red" and int(count) > MAX_RED:
                return False
            elif color == "green" and int(count) > MAX_GREEN:
                return False
    return True


def get_min_cubes(draws):
    min_red = 0
    min_blue = 0
    min_green = 0
    for d in draws:
        subdraw = d.split(", ")
        for sd in subdraw:
            count, color = sd.split()
            min_blue = int(count) if color == "blue" and int(count) > min_blue else min_blue
            min_red = int(count) if color == "red" and int(count) > min_red else min_red
            min_green = int(count) if color == "green" and int(count) > min_green else min_green
    return min_blue * min_red * min_green

def get_games_from_file(input):
    games = {}
    file = open(input, 'r')
    for line in file:
        gid, draws = parse_game_line(line)
        games[gid] = draws
    print(games)
    res = 0
    power = 0
    for i in games:
        power += get_min_cubes(games[i])
        if check_draws(games[i]):
           res += int(i)
    return res, power