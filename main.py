# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from day1 import get_sum_from_file
from day2 import get_games_from_file

if __name__ == '__main__':
    # print('Day 1')
    # res = get_sum_from_file('inputs/day1.txt')

    print('Day 2')
    res = get_games_from_file('inputs/day2.txt')
    print(res)
